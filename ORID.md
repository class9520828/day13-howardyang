O(Objective):
+ Today, I continued to practice and improve the functions of the Todo List project.
+ Learned how to use Router to navigate different web pages in a project.
+ I learned how to use the MockAPI, and then I used it to automatically generate multiple request connections and integrate the front and back ends.
+ Completed the replacement of operations such as adding, changing, deleting, and marking.
+ I learned to use Ant Design to add well-designed components and beautify the front-end interface.
+ I learned how to use asynchronous operations and applied them to the Todo List project.
+ Learned how to handle how to return error messages during webpage access.

R(Reflective):
+ Fruitful

I(Interpretive):
+ I am still confused about the transfer, update, and storage of data during the use of Axios. 
+ I am not very familiar with some of the content, functions, and keywords of asynchronous operations.

D(Decision):
+ Continue practicing and consolidating the knowledge learned today.
