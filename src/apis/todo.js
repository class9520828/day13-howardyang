import request from "./request";

export const loadTodos = () => {
    return request.get("/todos");
}

export const updateTodo = (id, done) => {
    return request.put(`/todos/${id}`, { done });
}

export const updateTodoText = (id, text) => {
    return request.put(`/todos/${id}`, { text: text });
}

export const createTodo = (todo) => {
    return request.post(`/todos`, todo)
}

export const deleteTodo = (id) => {
    return request.delete(`/todos/${id}`, id)
}

export const getTodoById = (id) => {
    return request.get(`/todos/${id}`, id)
}