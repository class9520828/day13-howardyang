import './App.css';
import './Components/TodoList.css'
import {
  RouterProvider
} from "react-router-dom";
import { router } from './router';

function App() {

  return (
    
    <div className='TodoList'>
      <RouterProvider router={router}></RouterProvider>
    </div>

  );

}

export default App;
