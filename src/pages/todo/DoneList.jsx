import { useSelector } from "react-redux";
import { TodoGroup } from "../../Components/TodoGroup/TodoGroup";

const DoneList = () => {
    const doneList = useSelector((state) =>
        state.todo.todoList)

    const doneListFilter = doneList.filter((todo) => {
        return todo.done;
    })

    return (<div>
        <h1>Done List</h1>
        <TodoGroup todoList={doneListFilter} isEdit={false} ></TodoGroup>
    </div>);
}

export default DoneList;