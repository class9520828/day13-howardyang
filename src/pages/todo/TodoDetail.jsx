import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { getTodoById } from "../../apis/todo";
import "./TodoDetail.css"
const TodoDetail = () => {
    const params = useParams();
    const navigate = useNavigate();
    const { id } = params;
    const [todo, setTodo] = useState({})

    useEffect(() => {
        getTodoById(id).then((response) => {
            setTodo(response.data)
        }
        )
        if (!todo) {
            navigate("/404");
        }
    }, [])

    return (
        <>
            <h1>Detail</h1>
            {todo && (
                <div className="detailText">
                    <span>{todo.text}</span>
                </div>
            )}
        </>
    );
}

export default TodoDetail;