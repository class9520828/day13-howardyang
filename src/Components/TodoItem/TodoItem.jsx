import { useDispatch } from "react-redux";
import { deleteTodo, updateTodoText, updateTodo } from "../../apis/todo"
import { useNavigate } from "react-router-dom";
import { Button, Modal, Input, Popconfirm } from "antd";
import { useState } from "react";
import "./TodoItem.css"
import {useTodo} from "../../hooks/useTodo"
export const TodoItem = (props) => {

    const navigate = useNavigate();

    const [open, setOpen] = useState(false);

    const [text, setText] = useState('');

    const { reloadTodos } = useTodo();

    const changeText = (inputText) => {
        setText(inputText)
    }

    const confirmEdit = async () => {
        if (props.isEdit) {
            await updateTodoText(props.todo.id, text)
            reloadTodos();
        }
        setOpen(false);
    };

    const cancelEdit = () => {
        setOpen(false);
    };

    const showModal = () => {
        setOpen(true);
    }

    const confirm = async (event) => {
        if (props.isEdit) {
            await deleteTodo(props.todo.id);
            reloadTodos();
            event.stopPropagation();
        }
    };
    const cancel = () => {

    };

    const setTodoDone = async () => {
        if (props.isEdit) {
            await updateTodo(props.todo.id, !props.todo.done)
            reloadTodos();
        } else {
            navigate(`/todo/${props.todo.id}`)
        }

    }

    return (
        <div className="todoItem">
            <span onClick={setTodoDone} className={props.todo.done && props.isEdit ? "todoItemText" : null}>{props.todo.text}</span>
            {props.isEdit && <>
                <Popconfirm title="Delete the task"
                    description="Are you sure to delete this todo?"
                    onConfirm={confirm}
                    onCancel={cancel}
                    okText="Yes"
                    cancelText="No">
                    <Button danger className="X">X</Button>
                </Popconfirm>
                <Button onClick={props.isEdit ? showModal : null} className="Edit" type="primary">Edit</Button>
                <Modal
                    title="EditText"
                    open={open}
                    onOk={confirmEdit}
                    onCancel={cancelEdit}
                    okText="OK"
                    cancelText="Cancel"
                >
                    <Input type="primary" onInput={(input) => changeText(input.target.value)} />
                </Modal>
            </>}

        </div>

    )

}

