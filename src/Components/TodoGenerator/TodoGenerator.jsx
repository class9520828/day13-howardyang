import { useState } from "react"
import { createTodo } from "../../apis/todo";
import { Input } from "antd";
import { Button } from "antd";
import "./TodoGenerator.css"
import { useTodo } from "../../hooks/useTodo";
export function TodoGenerator() {
    const [text, setText] = useState('');
    const { reloadTodos } = useTodo();
    const changeText = (inputText) => {
        setText(inputText)
    }

    const addInputTodo = async () => {
        if (text.trim() !== "") {
            await createTodo({
                text: text,
                done: false
            });
            reloadTodos();
        }
        setText("");
    }

    const handleKeyUp = (event) => {
        if (event.keyCode === 13) {
            addInputTodo();
        }
    }

    return (
        <div className="TodoGenerator">
            <Input type="primary" value={text} onInput={(input) => changeText(input.target.value)} onKeyUp={(event) => handleKeyUp(event)} placeholder="Please input here."/>
            <Button onClick={addInputTodo} type="primary">Add</Button>
        </div>
    )
}