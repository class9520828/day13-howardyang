import { TodoItem } from "../TodoItem/TodoItem"

export function TodoGroup(props) {

    return (
        <>
            {
                props.todoList.map((todo, index) => (<TodoItem key={index} todo={todo} isEdit={props.isEdit}></TodoItem>))
            }
        </>


    )
}