import { TodoGroup } from "./TodoGroup/TodoGroup"
import { TodoGenerator } from "./TodoGenerator/TodoGenerator"
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { useTodo } from "../hooks/useTodo"

export function TodoList() {
  const todoList = useSelector((state) => state.todo.todoList)
  const { reloadTodos } = useTodo();
  useEffect(() => {
    reloadTodos();
  }, [])
  return <>
    <div><h1>Todo List</h1></div>
    <TodoGroup todoList={todoList} isEdit={true}></TodoGroup>
    <TodoGenerator></TodoGenerator>
  </>

}