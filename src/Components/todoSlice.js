import { createSlice } from '@reduxjs/toolkit'

export const todoSlice = createSlice({
  name: 'todo',
  initialState: {
    todoList: []
  },
  reducers: {
    initTodos: (state, action) => {
        state.todoList = action.payload;
    },

    setDone: (state, action) => {
      const todoId = action.payload;
      const updateList = state.todoList.map(todo => {
        if (todo.id === todoId) {
          return {
            ...todo,
            done: !todo.done
          }
        }
        return todo
      })
      state.todoList = updateList
    },

  }
})

export const { setDone, initTodos } = todoSlice.actions;

export default todoSlice.reducer