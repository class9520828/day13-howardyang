import { Outlet } from "react-router-dom";
import { NavLink } from "react-router-dom";
const Layout = () => {
    return (<div>
        <div className="header">
            <NavLink to="/">Home</NavLink>
            <NavLink to="/done">Done</NavLink>
            <NavLink to="/about">About</NavLink>
        </div>
        <Outlet>
        </Outlet>

    </div>);
}

export default Layout;